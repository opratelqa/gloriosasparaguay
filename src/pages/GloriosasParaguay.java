package pages; 


import static org.testng.Assert.assertTrue;

import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JOptionPane;

//@FindBy(how = How.XPATH,using = "/html/body/div[4]/div/div/div/div[2]/div/a[1]")
//@FindBy(how = How.XPATH,using = "//a[contains(.,'Tr�mites')]")
//private WebElement btnTramites;


import javax.swing.ScrollPaneLayout;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.sun.javafx.collections.MappingChange.Map;

import okhttp3.Call;
import test.TestBase;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import test.TestBase;
import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import org.testng.Assert;
import org.testng.annotations.Test;
import okhttp3.Call;
import test.TestBaseTG;

//import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


 
public class GloriosasParaguay extends TestBaseTG {
	
	final WebDriver driver;
	public GloriosasParaguay(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	} 	
	
	 
	/*
	 ******PASAR A BASEPAGE 
	 */
	public void WaitForElementClickable(By element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void WaitForElementClickable(WebElement element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}	
	}
	
	public void cargando(int loadingPage) {
		
		//espera a que salga el elemento cargando
				try {
		    		waitForElementToDisappear(By.id("preloader"), 300, driver);
		    	} catch (Exception e) {
		    		System.out.println("No se encontro el Elemento: " + e.getMessage());
		    	}
		 
	}
	
	
	
	/*
	 ***************WEBELEMENTS*********** 
	 */

	@FindBy(how = How.CLASS_NAME,using = "form-control")
	private List<WebElement> campos;

	@FindBy(how = How.ID,using = "number")
	private WebElement IngresaLineaTelefonica;
	
	@FindBy(how = How.ID,using = "acepto")
	private WebElement checkMayorDeEdad;
	
	@FindBy(how = How.ID,using = "si")
	private WebElement btnContinue;
	
	

	
	
	//************CONSTRUCTOR***********
		
	@FindBy(how = How.ID,using = "RankBorrar")
	private WebElement aProbar;

	//*****************************

	
	public void logInGloriosasPy(String apuntaA) {		
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test Gloriosas Paraguay - landing");
		
	
			cargando(500);
			espera(500);
		WebElement menu2 = driver.findElement(By.xpath("//a[contains(text(), 'Soy mamá')]"));
			menu2.click();
			espera(500);
			
		driver.get( apuntaA + "gloriosas.com.py/category/moda-belleza/moda-gloriosa/");
			espera(500);
		
		driver.get( apuntaA + "gloriosas.com.py/category/hogar/mil-formas-de-reciclar/");
			espera(500);
			
		driver.get( apuntaA + "gloriosas.com.py/category/hogar/jardineria-facil/");
			espera(500);
		
		driver.get( apuntaA + "gloriosas.com.py/category/hogar/pequenos-consejos-grandes-soluciones/");
			espera(500);
			
		driver.get( apuntaA + "gloriosas.com.py/category/hogar/hogares-con-estilo/");
			espera(500);
			
		driver.get( apuntaA + "gloriosas.com.py/category/hogar/asi-cualquiera-cocina/");
			espera(500);
			
		driver.get( apuntaA + "gloriosas.com.py/category/dra-yuyera/");
			espera(500);
			
		driver.get( apuntaA + "gloriosas.com.py/category/vida-sana/salud/");
			espera(500);
			
		driver.get( apuntaA + "gloriosas.com.py/category/vida-sana/maldita-balanza/");
			espera(500);
			
		driver.get( apuntaA + "gloriosas.com.py/category/placeres/fotografia-gloriosa/");
			espera(500);
			
		driver.get( apuntaA + "gloriosas.com.py/category/placeres/sexo-y-pareja/");
			espera(500);
			
		driver.get( apuntaA + "gloriosas.com.py/category/placeres/cultura/");
			espera(500);
			
		driver.get( apuntaA + "gloriosas.com.py/category/placeres/descanso/");
			espera(500);
			
		//driver.get("");
			//espera(500);
			
		//driver.get("");
			//espera(500);
			
		WebElement menu3 = driver.findElement(By.xpath("//a[contains(text(), 'Gloriosas del Paraguay')]"));
			menu3.click();
			espera(500);
			
		WebElement menu4 = driver.findElement(By.xpath("//a[contains(text(), 'GLORIOSAS TV')]"));
			menu4.click();
			espera(500);

			
	/*		
		WebElement footer1 = driver.findElement(By.xpath("//a[contains(text(), 'sobre gloriosas')]"));
			footer1.click();
			espera(500);
			
		WebElement footer2 = driver.findElement(By.xpath("//a[contains(text(), 'términos y condiciones')]"));
			footer2.click();
			espera(500);
			
		WebElement footer3 = driver.findElement(By.xpath("//a[contains(text(), 'contacto')]"));
			footer3.click();
			espera(500);*/
	/*		
		driver.get( apuntaA + "");
			espera(500);
				
		driver.get( apuntaA + "");
			espera(500);
			
		driver.get( apuntaA + "");
			espera(500);
	*/		
			
		System.out.println("Resultado Esperado:Deberan visualizarce las secciones de Gloriosas Paraguay"); 
		System.out.println();
		System.out.println("Fin de Test Gloriosas Paraguay - Landing");
			
	}			
	
}  

